
import arcade


class Tile(arcade.Sprite):
    # Preload textures as static variables for all tiles
    tex_hit = arcade.load_texture("Pictures/hit.png")
    tex_sunk = arcade.load_texture("Pictures/sunk.png")
    tex_tile = arcade.load_texture("Pictures/tile.png")
    tex_blue = arcade.load_texture("Pictures/tile_blue.png")
    tex_selected = arcade.load_texture("Pictures/tile_selected.png")

    def __init__(self, is_revealed, tex, x, y, scale=1):
        self.is_revealed = is_revealed
        super().__init__(None, scale, center_x=x, center_y=y, texture=tex)

    def change_skin(self, tex):
        self.texture = tex

    def change_color(self, color):
        self.color = color

    # Maybe use these and deal with uncover logic in the play_view?
    def miss(self):
        self.texture = Tile.tex_blue

    def hit(self):
        self.texture = Tile.tex_hit

    def sunk(self):
        self.texture = Tile.tex_sunk

    def selected(self):
        self.texture = Tile.tex_selected

    def uncover(self):
        self.is_revealed = True
        self.color = arcade.color.SAPPHIRE
        # self.change_skin(Tile.tex_hit)
