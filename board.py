import arcade

import constants
from constants import *
from ship import Ship
from tile import Tile


class Board:
    # Need to figure out whether dimensions will be a tuple or something else

    def __init__(self, dimensions, offset_x, offset_y):
        # Dimensions, ships and tiles are instance variables
        self.dimensions = dimensions  # Int tuple
        self.ships_list = arcade.SpriteList()  # List of Ship Sprites
        self.tiles_list = arcade.SpriteList()  # List of Tile Sprites in the grid
        self.tiles_grid = []  # Grid of Tile Sprites
        self.ships_list.append(Ship("Pictures/Carrier.png", 5, 595, 340, SCALE_CARRIER))
        self.ships_list.append(Ship("Pictures/Destroyer.png", 2, 530, 395, SCALE_DESTROYER))
        self.ships_list.append(Ship("Pictures/Submarine.png", 3, 530, 275, SCALE_SUBMARINE))
        self.ships_list.append(Ship("Pictures/Cruiser.png", 3, 530, 140, SCALE_CRUISER))
        self.ships_list.append(Ship("Pictures/Battleship.png", 4, 595, 150, SCALE_BATTLESHIP))

        # Create Tiles and make grid
        for row in range(self.dimensions[0]):
            self.tiles_grid.append([])
            for col in range(self.dimensions[1]):
                x = col * (TILE_DIMENSION + MARGIN) + (TILE_DIMENSION / 2 + MARGIN) + offset_x
                y = row * (TILE_DIMENSION + MARGIN) + (TILE_DIMENSION / 2 + MARGIN) + offset_y
                tile = Tile(False, Tile.tex_tile, x, y, .3)
                self.tiles_list.append(tile)
                self.tiles_grid[row].append(tile)

    def loss(self):
        for ship in self.ships_list:
            if not ship.sunk:
                return False
        return True

    def hide_ships(self):
        for ship in self.ships_list:
            ship.visible = False

    # Checks if the center of each ship is within the x,y boundaries of the top left and bottom right tile
    def is_ready(self):
        for i in self.ships_list:
            if i.center_x < self.tiles_list[0].center_x or i.center_x > self.tiles_list[-1].center_x:
                return False
            if i.center_y < self.tiles_list[0].center_y or i.center_y > self.tiles_list[-1].center_y:
                return False
        return True
