import asyncio
import socketio

isConnected = False
io = socketio.Client()


def connect_socket():
    try:
        io.connect("https://cs205-battleship.herokuapp.com/")
    except:
        print("Failed to connect to Server")
    # io.wait()


def disconnect_socket():
    io.disconnect()


@io.event
def connect():
    global isConnected
    isConnected = True
    print("Connection established")


@io.event
def message(data):
    print("I did it")


@io.event
def disconnect():
    global isConnected
    isConnected = False
    print("Socket disconnected")
