
import arcade
from constants import TILE_DIMENSION, MARGIN


class Ship(arcade.Sprite):
    def __init__(self, texture_file, size, init_x, init_y, scale=1):
        # Ship Attributes (Instance Variables)
        super().__init__(texture_file, scale, hit_box_algorithm="None")
        self.hits = 0
        self.sunk = False
        self.size = size
        self.init_x = init_x
        self.init_y = init_y
        self.position = (init_x, init_y)

    def hit(self):
        self.hits += 1
        if self.size == self.hits:
            self.sunk = True

    def rotate(self):
        self.turn_right(90.0)
        if self.angle < -270:
            self.angle = 0

    def is_sunk(self):
        return self.sunk

    # Move to game_view cause only used in game_view. game view if name change is setup view
    def snapping(self, tiles_list):
        closest = arcade.get_closest_sprite(self, tiles_list)
        half_tile_dist = (TILE_DIMENSION + MARGIN) / 2  # HALF TILE + MARGIN
        x = closest[0].center_x
        y = closest[0].center_y
        if self.size % 2 == 0:
            if self.angle == -90 or self.angle == -270:
                x += half_tile_dist
            else:
                y += half_tile_dist
        self.position = (x, y)
        # Check for ship being off board
        top = tiles_list[-1].top
        left = tiles_list[0].left
        right = tiles_list[-1].right
        bottom = tiles_list[0].bottom
        if self.top > top:
            self.top = top + 1
        if self.bottom < bottom:
            self.bottom = bottom - 1
        if self.right > right:
            self.right = right + 1
        if self.left < left:
            self.left = left - 1
