import arcade 
# Not Implemented into other files yet.

# Ship Constants
SCALE_BATTLESHIP = .31
SCALE_CARRIER = .55
SCALE_CRUISER = .2
SCALE_DESTROYER = .11
SCALE_SUBMARINE = .3
SHIP_WIDTH = 30  # Not used rn

# Tile Constants
TILE_DIMENSION = 34
MARGIN = 2
SCALE_TILE = 3.0  # Not used till tiles get rescaled in the images.

# Window
window = arcade.Window(1000, 500, "Battleship")
