
import arcade
from start_view import StartScreen
import network_handler as game_socket
from constants import window


def main():
    start = StartScreen()
    window.show_view(start)
    arcade.run()


@window.event
def on_close():
    game_socket.disconnect_socket()
    arcade.exit()


if __name__ == "__main__":
    main()  
