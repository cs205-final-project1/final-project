import arcade
import arcade.gui
import random
from board import Board
from end_view import EndScreen


class Battle(arcade.View):
    def __init__(self, player_board: Board, opponent_board: Board):
        super().__init__()
        self.player_board = player_board
        self.opponent_board = opponent_board
        self.my_turn = None  # Will have to move to server side
        self.tile_clicked = None
        self.ship_clicked = None
        self.end_view = EndScreen()


    def setup(self):
        self.my_turn = True  # Needs to be server side
        self.tile_clicked = []
        self.ship_clicked = []
        self.opponent_board.hide_ships()

    def on_mouse_press(self, x: int, y: int, button: int, modifiers: int):
        # my_turn checks that it's the players turn before any action can be taken
        if self.my_turn:
            if button == arcade.MOUSE_BUTTON_LEFT:
                self.tile_clicked = arcade.get_sprites_at_point((x, y), self.opponent_board.tiles_list)
                self.ship_clicked = arcade.get_sprites_at_point((x, y), self.opponent_board.ships_list)
                if len(self.tile_clicked) != 0:
                    if len(self.ship_clicked) != 0 and not self.tile_clicked[0].is_revealed:
                        self.ship_clicked[0].hit()
                        self.tile_clicked[0].hit()
                    else:
                        self.tile_clicked[0].miss()
                # Todo: Put in actual check for game over and remove this when server side stuff completed
                if self.opponent_board.loss():
                    self.window.show_view(self.end_view)
                # self.my_turn = False
                # self.opponent_move()

    def on_draw(self):
        self.clear()
        arcade.draw_text("Your Board", self.window.width / 2 - 225, self.window.height / 2 + 200,
                         arcade.color.WHITE, font_size=18, anchor_x="center")
        arcade.draw_text("Opponent Board", self.window.width / 2 + 200, self.window.height / 2 + 200,
                         arcade.color.WHITE, font_size=18, anchor_x="center")
        self.player_board.tiles_list.draw()
        self.player_board.ships_list.draw()
        self.opponent_board.tiles_list.draw()
        self.opponent_board.ships_list.draw()

    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == arcade.key.A:
            if symbol == arcade.key.A:
                end_view = EndScreen()
                self.window.show_view(end_view)

    def opponent_move(self):
        valid = False
        while not valid:
            x = random.randint(0, 99)
            y = random.randint(0, 99)
            tile_guessed = arcade.get_sprites_at_point((x, y), self.player_board.tiles_list)
            if not tile_guessed[0].is_revealed:
                tile_guessed[0].uncover()
                valid = True
                self.my_turn = True
                return tile_guessed[0]
