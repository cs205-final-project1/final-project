import asyncio
import arcade
import arcade.gui
from setup_view import SetupView
import network_handler as game_socket


class StartScreen(arcade.View):
    def on_show_view(self):
        game_socket.connect_socket()
        arcade.set_background_color(arcade.csscolor.DARK_SLATE_BLUE)
        arcade.set_viewport(0, self.window.width, 0, self.window.height)

    def on_draw(self):
        network_status = game_socket.isConnected
        self.clear()
        arcade.draw_text("Instructions Screen", self.window.width / 2, self.window.height / 2,
                         arcade.color.WHITE, font_size=50, anchor_x="center")
        arcade.draw_text("Click to advance", self.window.width / 2, self.window.height / 2-75,
                         arcade.color.WHITE, font_size=20, anchor_x="center")
        if network_status:
            arcade.draw_text("Online", self.window.width - 55, self.window.height - 485,
                             arcade.color.GREEN, font_size=20, anchor_x="center")
        else:
            arcade.draw_text("Offline", self.window.width - 55, self.window.height - 485,
                             arcade.color.RED, font_size=20, anchor_x="center")

    def on_mouse_press(self, _x, _y, _button, _modifiers):
        setup_view = SetupView()
        setup_view.setup()
        self.window.show_view(setup_view)
