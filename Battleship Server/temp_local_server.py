import socketio
import eventlet

# Create socketIO Server
io = socketio.Server()

# Runs the WSGI server
def run_server():
    app = socketio.WSGIApp(io)
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)
    print("Server closed.")

# Collect x and y data from analog to digital converter, then emit it

@io.event
def my_event(sid):
    pass

# Define main
def main():
    run_server()


# Run main method
if __name__ == '__main__':
    main()