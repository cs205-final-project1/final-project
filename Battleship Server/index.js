#!/opt/nodejs/v12/bin/node
const app = require('express')();
const http = require('unit-http').Server(app);

app.get('/', function(req, res) {
   res.sendFile("static/index.html");
});

http.listen();