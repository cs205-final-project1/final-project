import arcade.gui
from network_handler import disconnect_socket

class QuitButton(arcade.gui.UIFlatButton):
    def on_click(self, event: arcade.gui.UIOnClickEvent):
        disconnect_socket()
        arcade.exit()

