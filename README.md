# Instruction
- Run Main
- "R" Key rotates ships
- Click and drag ships around
    - Placement needs to be ironed out but works for now
- If something was missed the views should have on_key_pressed buttons that contain the controls
- Pressing A on the ship placement screen will take you to the end screen


# Packages
- arcade
- python-socketio
- requests
- websocket-client

# Grade Branch: SEPARATE_FILES