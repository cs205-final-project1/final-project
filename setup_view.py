
import arcade
import arcade.gui
from board import Board
from battle_view import Battle
from end_view import EndScreen
from constants import window
import network_handler as game_socket

green_style = {
    "font_name": ("calibri", "arial"),
    "font_size": 15,
    "font_color": arcade.color.BLACK,
    "border_width": 2,
    "border_color": None,
    "bg_color": arcade.color.FOREST_GREEN,
}


class SetupView(arcade.View):
    def __init__(self):
        super().__init__()
        self.player_board = None
        self.opponent_board = None
        self.session_id = ''
        self.game_state = "setup"
        self.ship_list = None
        self.ship_held = None
        self.tiles_highlighted = None
        self.opponent_board = None
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        self.v_box = arcade.gui.UIBoxLayout()
        ready_button = arcade.gui.UIFlatButton(
            text="Ready", width=200, style=green_style)
        self.v_box.add(ready_button.with_space_around(bottom=20))
        ready_button.on_click = self.on_click_ready

        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center",
                align_x=300,
                anchor_y="center",
                child=self.v_box)
        )

    def setup(self):
        self.player_board = Board((10, 10), 100, 75)
        self.opponent_board = Board((10, 10), 525, 75)
        #  self.ship_list = self.player_board.ships_list
        self.ship_held = []
        self.tiles_highlighted = arcade.SpriteList()

    def on_mouse_press(self, x: int, y: int, button: int, modifiers: int):
        print(f"You clicked at x:{x} and y:{y}")

        if button == arcade.MOUSE_BUTTON_LEFT:
            self.ship_held = arcade.get_sprites_at_point(
                (x, y), self.player_board.ships_list)
            if len(self.ship_held) > 0:
                self.ship_held[0].position = (x, y)

        if button == arcade.MOUSE_BUTTON_RIGHT:
            if len(self.ship_held) > 0:
                self.ship_held[0].rotate()
                print(f"Ship angle:{self.ship_held[0].angle}")

        if button == arcade.MOUSE_BUTTON_MIDDLE:
            for ship in self.player_board.ships_list:
                print(ship.position)
            # if len(self.ship_held > 0):
            #     self.ship_held[0].visible = False

    def on_mouse_release(self, x: int, y: int, button: int, modifiers: int):
        if button == arcade.MOUSE_BUTTON_LEFT:
            if len(self.ship_held) > 0:
                self.ship_held[0].snapping(self.player_board.tiles_list)
                for tile in self.player_board.tiles_list:
                    tile.color = arcade.color.WHITE
            self.ship_held = []
            for tile in self.tiles_highlighted:
                tile.color = arcade.color.WHITE

    def on_mouse_motion(self, x: int, y: int, dx: int, dy: int):
        if len(self.ship_held) > 0:
            for ships in self.ship_held:
                ships.center_x += dx
                ships.center_y += dy

            # Scuffed way of highlighting ship placement.
            for tile in self.player_board.tiles_list:
                tile.color = arcade.color.WHITE
            for tile in arcade.check_for_collision_with_list(self.ship_held[0], self.player_board.tiles_list):
                tile.color = arcade.color.FUCHSIA

    # Maybe use snapping to highlight tiles
    def highlight_tiles(self):
        # Reset All Tiles to White
        for tile in self.player_board.tiles_list:
            tile.change_color(arcade.color.WHITE)

        # Get closest tile
        min_row = 0
        min_col = 0
        min = 10000
        for col in range(self.player_board.dimensions[0]):
            tiles = self.player_board.tiles_grid[col]
            for row in range(len(tiles)):
                dist = arcade.get_closest_sprite(self.ship_held[0], tiles)[1]
                if min > dist:
                    min = dist
                    min_col = col
                    min_row = row

        if self.ship_held[0].angle == -90 or self.ship_held[0].angle == -270:
            if self.ship_held[0]:
                pass

        # Option 2 rn
        close = arcade.get_closest_sprite(
            self.ship_held[0], self.tiles_highlighted)
        closer = arcade.SpriteList()
        for tile in self.tiles_highlighted:
            if self.ship_held[0].angle == -90 or self.ship_held[0].angle == -270:
                if tile.center_y == close[0].center_y:
                    closer.append(tile)
            else:
                if tile.center_x == close[0].center_x:
                    closer.append(tile)

        # Highlight closest tiles
        for tile in range(self.ship_held[0].size):
            index = closer.index(arcade.get_closest_sprite(
                self.ship_held[0], closer)[0])
            closer.pop(index).color = arcade.color.FUCHSIA

    def on_key_press(self, symbol: int, modifiers: int):
        if symbol == arcade.key.R:
            if len(self.ship_held) > 0:
                self.ship_held[0].rotate()
                print(f"Ship angle:{self.ship_held[0].angle}")
        if symbol == arcade.key.A:
            end_view = EndScreen()
            self.window.show_view(end_view)

    def on_show(self):
        arcade.set_viewport(0, self.window.width, 0, self.window.height)
        arcade.set_background_color(arcade.csscolor.DARK_SLATE_BLUE)

    def on_draw(self):
        self.clear()
        # arcade.start_render()  # works instead of clearing everytime. Might technically be the correct way?
        # self.ship_list.draw()
        self.player_board.tiles_list.draw()
        arcade.draw_rectangle_filled(
            563, 255, 120, 355, arcade.color.AMARANTH_PURPLE)
        self.player_board.ships_list.draw()
        arcade.draw_text("Place Your Ships", self.window.width / 2, 450,
                         arcade.color.WHITE, font_size=30, anchor_x="center")
        self.manager.draw()

    def temp_set_opponent_ships(self):
        # For testing purposes only
        for i in range(5):
            self.opponent_board.ships_list[i].position = self.player_board.ships_list[i].position
            self.opponent_board.ships_list[i].angle = self.player_board.ships_list[i].angle
        self.opponent_board.ships_list.move(425, 0)

    def on_click_ready(self, event: arcade.gui.UIOnClickEvent):
        self.temp_set_opponent_ships()
        battle_view = Battle(self.player_board, self.opponent_board)
        battle_view.setup()
        self.window.show_view(battle_view)
